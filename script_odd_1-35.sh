#!/bin/bash
while [[ 1 ]]
do
    echo -ne "\n\033[32m
    Bash!
    1) Вывести информацию о том, какой дистрибутив Linux используется
    3) Изменить пароль пользователя через терминал
    5) Вывести только список папок в домашней директории пользователя
    7) Создать скрипт, который выводит “Hello World” и ограничить доступ НЕ применяя chown, чтобы никто из пользователей системы (кроме root пользователя) НЕ смог выполнить этот скрипт при условии, что скрипт уже исполняемый. Вернуть доступ к файлу для пользователей после выполнения задания
    9,11) Дополнить скрипт, чтобы он проверял, установлен ли в системе пакет vim и установил его в случае отсутствия без вывода подробностей установки, и в конце написать “Completed” \nОбрезать строку “Hello World”, чтобы остались только буквы “llo Wo” без использования команды cut
    13) Изменить поведение ввода пользовательских данных, чтобы они передавались как аргумент скрипта. Сделать проверку на отсутствие второго аргумента и прерывать выполнение в случае его отсутствия. Вывести аргумент (аргументы, если их несколько) в консоль
    15) Переписать предыдущее задание, чтобы можно было вызвать первую фразу с помощью короткого (-l) или длинного (--long) флага, а вторую опцию только с помощью длинного (--sparta-flag) флага.
    17,19,21,23) look in manual
    25) Скачать файлы проекта из удалённого репозитория, не используя Git
    27) Заархивировать проект в tar архив и в tar.gz архив
    29) Сделать cron-задачу, которая будет каждую минуту синхронизировать (не копировать!) данный архив в какой-либо другой папке при условии, что архив был изменён. После выполнения, проверить работу, создав архив с какими-либо изменениями в проекте, подождав минуту и открыв синхронизируемый архив в целевой папке. При этом логи выполнения синхронизации должны записываться в отдельный файл
    31) Находясь в домашней директории пользователя найти в файловой системе слово “trello”, выводя в консоль местонахождение файла, номер строки и саму строку в целевом файле
    33) Изменить оболочку по умолчанию на shell и вернуть обратно на bash
    35) Скачать JSON-файл отсюда и спарсить его, чтобы он выводил только значения полей “species”
    0) Exit
    \n"
    
    echo -e "\033[0m\n\033[0m\033[31m \nMake your choose: "
    read -r choice
    case $choice in 
    1)
        uname -v
        ;;
    3)
        read -p "Введите имя пользователя: " user_name
        passwd $user_name
        ;;
    5)
        read -p "Введите имя пользователя: " user_name
        ls -d /home/$user_name/*/
        ;;
    7)
        if [[ $USER != 'root' ]]; 
        then
            echo "You are not root"
        else
            echo "Hello World"
        fi
        ;;
    9,11)
        if [[ $USER != 'root' ]]; 
        then
            echo "You are not root"
        else
            if [ "$(dpkg -l | grep -qw vim || apt-get -q install vim)" != "" ];
            then
                echo "Completed!"
            else
                echo "Completed!"
            fi
            echo "Hello World"
            echo "Hello World" | sed -e 's/Hello/llo/; s/World/Wo/'
        fi
        ;;
    13)
        if [[ $1 == '' ]] || [[ $2 == '' ]];
        then
        echo -n "Enter login: "
        read login
            if [[ $login == '' ]];
            then
                echo "You did not enter your login"
                exit 0
            else
                echo -n "Enter password: "
                read password
                if [[ $password == '' ]];
                then
                    echo "You did not enter your password"
                    exit 0
                else
                    echo "Welcome, $login  your password is $password!"
                fi
            fi
        else
            echo "Welcome, $1 your password is $2!"
        fi
        ;;
    15)
        if [[ $1 != '-l' ]] && [[ $2 != '-l' ]] && [[ $1 != '--long' ]] && [[ $2 != '--long' ]];
        then
        :
        else
            if [[ $2 == '--sparta-flag' ]] && [[ $1 == '-l' ]];
            then
                echo "This is \\// Sparta!"
            else
                if [[ $1 == '-l' ]] || [[ $1 == '--long' ]];
                then
                    echo "This is my flat"
                else
                    echo $((1 + $RANDOM % 10))
                fi
            fi
        fi
        ;;
    17,19,21,23)
        function Sh {
        if [ -f ./files/file.txt ];
        then
            while read -r line
            do
                echo -e "$line"
            done < ./files/file.txt
            D=$(date  +%d-%m-%Y\ %T)
            echo "$D"

            if [ $? -eq 0 ]
            then
                echo "Bye!"
            else
                echo "Bye!" >&2
            fi
        fi
        }
        Sh
        ;;
    25)
        wget --no-check-certificate --content-disposition https://raw.githubusercontent.com/miguelgrinberg/flask-examples/master/01-hello-world/hello.py
        ;;
    27)
        tar -cvf ./files/Project.tar ./files/*.py
        tar -cvzf ./files/Project.tar.gz ./files/*.py
        ;;
    29)
        rsync -azvh ./files/Project.tar ~/Downloads/ >> /home/logs/cron.log
        rsync -azvh ./files/Project.tar.gz ~/Downloads/ >> /home/logs/cron.log
        echo "* * * * * ./Cron_local.sh" >> /etc/crontab
        ;;
    31)
        grep -nsr "trello" /*
        ;;
    33)
        echo "Use 'echo $0'"
        echo "
        sh - Для смены на sh\n
        bash - для возврата на Bash "
        ;;
    35)
        wget https://raw.githubusercontent.com/LearnWebCode/json-example/master/animals-1.json
        cat animals-1.json | grep species
        ;;
    0)
        echo "Bye!"
        exit 1
        ;;
    *)
        echo "Wrong option. Please select right chosee!"
        ;;
    esac
done
