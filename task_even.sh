#!/bin/bash
while [[ 1 ]]
do
    echo -ne "\n\033[32m
    Bash!
    2) Вывести информацию о том, какое ядро используется данным дистрибутивом
    4) Вывести список всех разделов
    6) Посмотреть сколько занимают места все файлы и папки в домашней директории пользователя и сортировать его по возрастанию
    8) Включить отладку в скрипте + 10(Дополнить скрипт, чтобы он разделял слова “Hello” и “World” с помощью ASCII-символа)
    12) 12 и 13 таски это мегатрон, так исторически сложилось. (12)Дополнить скрипт, чтобы он давал возможность вводить пользовательские данные и передавать скрипту с каким-либо
    сообщением.(13) Изменить поведение ввода пользовательских данных, чтобы они передавались как аргумент скрипта. Сделать проверку на отсутствие второго аргумента и прерывать выполнение в случае его отсутствия. Вывести аргумент (аргументы, если их несколько) в консоль
    14) Дополнить скрипт так, чтобы при вводе пользовательских данных (вне зависимости от способа) выводился определённый текст. При вводе цифры 1 выводится текст “This is my flat”. При вводе цифры 2 выводится текст “This is \\// Sparta!”, а при любом другом аргументе – случайное число
    16) Дополнить скрипт, написав логику проверки на совпадение введённых дважды имени и пароля, используя один оператор проверки.
    18) Создать пустой файл и наполнить его многострочным текстом, используя команду cat(введите текст, после нажмите ctrl + D, будет создан файл test.txt)
    20) Дописать скрипт, позволяющий вывести в конце exit code скрипта и его PID
    22) Инициализировать массив из 5 элементов и вывести каждый из них с новой строки
    24) Написать алиас, позволяющий вызвать эту функцию
    26) Развернуть проект. Запустить проект в фоновом режиме, используя systemd. Удостовериться, что после перезагрузки локальной машины процесс запустится сам
    28) Разархивировать обычный и сжатый архив
    30) Найти файл requirements.txt в файловой системе, находясь в домашней директории пользователя
    32) Сделать предпросмотр изменений файла requirements.txt без его непосредственного изменения, используя команду sed
    34) Создать SSH-ключ с паролем
    36) Создать простой HTML-файл с вёрсткой заголовка <h2>, которая будет выводить “Hello World” жирный шрифтом по центру строки. Установить веб-сервер NGINX локально. Раздать этот статический файл так, чтобы до него можно было достучаться по домену “mytestproject.local”
    0) Exit "

    echo -e "\033[0m\n\033[0m\033[31m \nMake your choose: "
    read -r choice
    case $choice in
    2)
        uname -sr
        ;;
    4)
        df -h
        ;;
    6)
        ls --sort=size -alhr /home/$USER
        ;;
    8)
        set -x #on
        if [[ $USER != 'root' ]];    # 7 задание
        then
        echo "You are not root"
        else
        printf "Hell""\x6f ""Worl""\x64""\n"  # 10 задание
        fi
        set +x #of
        ;;
    12)
        if [[ $1 == '' ]] || [[ $2 == '' ]];    # 13 задание
        then
        echo -n "Enter login: "
        read login
        if [[ $login == '' ]];  # 12 задание
        then
        echo "You did not enter your login"
        exit 0
        else
        echo -n "Enter password: "    # 12 задание
        read password
        if [[ $password == '' ]];
        then
        echo "You did not enter your password"
        exit 0
        else
        echo "Welcome, $login  your password is $password!"   # 12 задание
        fi
        fi
        else
        echo "Welcome, $1 your password is $2!"   # 13 задание
        fi
        ;;
    14)
        if [[ $1 == '1' ]] || [[ $2 == '1' ]]; then
        echo "This is my flat"
        else if [[ $1 == '2' ]] || [[ $2 == '2' ]]; then
        echo "This is \\// Sparta!"
        else
        echo $((1 + $RANDOM % 10))
        fi
        fi
        ;;
    16)
        if [[ $1 == $2 ]]; then
        echo "error login = password"
        exit 0
        fi
        ;;
    18)
        cat>test.txt
        ;;
    20)
        echo "pid: "$$
        echo "exit code: "$?
        ;;
    22)
        array=(1 2 4 8 16)
        for i in ${array[@]};
         do
        echo $i
        done
        ;;
    24)
        alias 24_sh="bash script_17.sh" && ${BASH_ALIASES[24_sh]}
        ;;
    26) echo "for start app: python3 hello.py"
        echo "for create service: change paths in hello.service file;
        add hello.service file to /etc/systemd/system/"
        echo "for start service: sudo systemctl daemon-reload;
        sudo systemctl start hello;
        sudo systemctl enable hello;
        sudo systemctl restart hello"
        ;;
    28) mkdir simple && mv ./files/Project.tar ./simple/Project.tar && cd ./simple && tar -xvf ./Project.tar
        cd .. && mkdir compressed && mv ./files/Project.tar.gz ./compressed/Project.tar.gz && cd ./compressed && tar xvzf ./Project.tar.gz
        ;;
    30) find /home/$USER/ -name requirements.txt
        ;;
    32) command="find /home/$USER/ -name requirements.txt"
        path=$($command)
        echo "_______before________"
        cat $path
        echo "_______after________"
        sed 's/Flask/Pony/' $path
        ;;
    34) ssh-keygen -t rsa -f test-key -q -P "test"
        ;;
    36) echo "index.html search in project"
        ;;
    0)
        exit 1
        ;;
    *)
        echo "Wrong option. Please select right chosee!"
        ;;
    esac
done
