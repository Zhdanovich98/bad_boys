# First project #
### ✨Bash✨ ###

1. Файл **task_even.sh** содержит чётные задания по Bash. Их делал Илья.

2. Файл **script_odd_1-35.sh** содержит нечётные задания по Bash, их делал Дима.

3. Файл **script_17.sh** содержит 17-й пунк задания, на который при работе ссылаются и **task_even.sh** и **script_odd_1-35.sh**.

##  Для проверки заданий: ##

Запустить файл **task_even.sh** либо **script_odd_1-35.sh**. Скрипт даст возможность выбрать номер задания, что запустит его.

## Описание содержимого каталога files: ##


**Project.tar, Project.tar.gz** - 27 задание.

**Cron_local.sh** - 29 задание.

**hello.service** - 26 задание.

**file.txt** - 17/18 задание.

**index.html** - 36 задание.

**hello.py** - 26 задание.

___

  Следующий решённый раздел GIT находится на ветках  [Git_D](https://gitlab.com/Zhdanovich98/bad_boys/-/refs/switch?destination=tree&path=&ref=Git_D) (Дима) и  [Git_I](https://gitlab.com/Zhdanovich98/bad_boys/-/refs/switch?destination=tree&path=&ref=git_I) (Илья)

___
