#!/bin/bash
# 17 и 19 и 21 и 23 задание:
function Sh {

    if [ -f ./files/file.txt ];
    then
        while read -r line
        do
            echo -e "$line"
        done < ./files/file.txt
        D=$(date  +%d-%m-%Y\ %T)
        echo "$D"

        if [ $? -eq 0 ]
        then
            echo "Bye!"
        else
            echo "Bye!" >&2
        fi
    fi
}

Sh